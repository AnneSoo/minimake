CC= gcc
CFLAGS= -Wextra -Wall -Werror -std=c99 -pedantic -g -fsanitize=address -fsanitize=undefined
EXEC= minimake
EXECTEST= test
SRC= src/minimake.c src/choose.c src/linled_list.c src/parse.c src/exec_no_arg.c src/exec_arg.c
SRCTEST= tests/test_strcmp.c

all:
	$(CC) $(CFLAGS) $(SRC) -o $(EXEC)

check:
	$(CC) $(CFLAGS) $(SRCTEST) -o $(EXECTEST)
	./$(EXECTEST)

clean:
	rm $(EXEC)
	rm $(EXECTEST)
