#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "minimake.h"

FILE* choose_makefile(char *file)
{
    if (strcmp(file, "") == 0)
    {
        FILE *f = fopen("makefile", "r");
        if (!f)
        {
            f = fopen("Makefile", "r");
        }
        return f;
    }
    else
    {
        FILE *f = fopen(file, "r");
        return f;
    }
}
