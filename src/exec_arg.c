#include <stdio.h>
#include <string.h>

#include "minimake.h"
#include "noarg.h"
#include "arg.h"

int execArg(struct makefile *mf, char *args[])
{
    int i = 1;
    char *target = args[i];
    int result = 1;
    if (mf->rules->next)
    {
        while (target)
        {
            if (strcmp(target, "-f") == 0)
            {
                i = i + 2;
                target = args[i];
            }
            else
            {
                result = exec_rule(target, mf->rules);
                if (result)
                    return result;
                ++i;
                target = args[i];
            }
        }
    }
    else
        fprintf(stderr, "No rule found int the makefile\n");
    return result;
}
