#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>


#include "minimake.h"
#include "noarg.h"

int exec_cmd(char *cmd)
{
    char *argv[] = {"/bin/sh", "-c", cmd, NULL};
    pid_t pid;
    int status;
    if ((pid = fork()) == -1)
    {
        fprintf(stderr, "Error when trying to fork\n");
        return 1;
    }
    else if (pid == 0)
        execv(argv[0], argv);
    else
    {
        waitpid(pid, &status, 0);
        if (status != 0)
            return 2;
        return 0;
    }
    return 0;
}

int search_rule(char *target, struct rule *rl)
{
    struct rule *tmp = rl;
    while (tmp && (strcmp(tmp->target, target) != 0))
        tmp = tmp->next;
    if (!tmp)
        return 0;
    return 1;
}

char* remove_comments(char *cmd)
{
    char *c = cmd;
    while ((*c != '#') && (*c != '\0'))
        c++;
    *c = '\0';
    return cmd;
}

int exec_rule(char *target, struct rule *rl)
{
    struct ndep *dep = NULL;
    struct rule *tmp_rl = rl;
    struct nrecip *rec = NULL;
    int found_rl = 0;
    int result = 0;
    int flag = 0;
    if (!target)
    {
        fprintf(stderr, "minimake: no target to build\n");
        return 1;
    }
    while (tmp_rl && (strcmp(tmp_rl->target, target) != 0))
        tmp_rl = tmp_rl->next;
    if (!tmp_rl)
    {
        fprintf(stderr, "No target \"%s\" found in the makefile\n", target);
        return 1;
    }
    if (tmp_rl->done == 1)
    {
        printf("minimake: '%s' is up to date.\n", target);
        return 0;
    }
    tmp_rl->done = 1;
    dep = tmp_rl->dependencies->next;
    while (dep && dep->depend)
    {
        found_rl = search_rule(dep->depend, rl);
        if (found_rl)
        {
            result = exec_rule(dep->depend, rl);
            if (result && (result != 15))
                return result;
        }
        else
        {
            FILE *f = fopen(dep->depend, "r");
            if (f)
            {
                fclose(f);
            }
            else
            {
                fprintf(stderr, "The dependency \"%s\" of target \"%s\" is ",
                        dep->depend, target);
                fprintf(stderr, "not corresponding to any target in the ");
                fprintf(stderr, "makefile or any existing file\n");
                return 1;
            }
        }
        dep = dep->next;
    }
    rec = tmp_rl->recipe->next;
    if (!rec || !rec->cmd)
    {
        printf("minimake: Nothing to be done for \'%s\'\n", tmp_rl->target);
        return result;
    }
    while (rec)
    {
        if (rec->cmd[0] != '@')
            printf("%s\n", rec->cmd);
        else
        {
            flag = 1;
            rec->cmd = rec->cmd + 1;
        }
        fflush(stdout);
        fflush(stderr);
        result = exec_cmd(remove_comments(rec->cmd));
        if (flag == 1)
            rec->cmd = rec->cmd - 1;
        if (result)
            return result;
        rec = rec->next;
        flag = 0;
    }
    return result;
}

int execNoArg(struct makefile *mf)
{
    struct rule *rl = mf->rules->next;
    if (!rl)
    {
        fprintf(stderr, "No rule found int the makefile\n");
        return 1;
    }
    int result = exec_rule(rl->target, rl);
    return result;
}
