#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "minimake.h"

void add_var(struct var *list, struct var vari)
{
    if (!list)
        return;
    else
    {
        struct var *tmp = list;
        struct var *new = calloc(1, sizeof(struct var));

        while (tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        new->alias = vari.alias;
        new->value = vari.value;
        new->next = NULL;
        tmp->next = new;
    }
}

void init_var(struct var *vv)
{
    vv->alias = malloc(5 * sizeof(char));
    strcpy(vv->alias, "head");
    vv->value = malloc(5 * sizeof(char));
    strcpy(vv->value, "head");
    vv->next = NULL;
}

void init_rec(struct nrecip *vv)
{
    vv->cmd = malloc(5 * sizeof(char));
    strcpy(vv->cmd, "head");
    vv->next = NULL;
}

void init_dep(struct ndep *vv)
{
    vv->depend = malloc(5 * sizeof(char));
    strcpy(vv->depend, "head");
    vv->next = NULL;
}

void init_rul(struct rule *vv)
{
    vv->target = malloc(5 * sizeof(char));
    strcpy(vv->target, "head");
    vv->dependencies = malloc(sizeof(struct ndep));
    init_dep(vv->dependencies);
    vv->recipe = malloc(sizeof(struct nrecip));
    init_rec(vv->recipe);
    vv->next = NULL;
}

void add_recipe(struct nrecip *list, struct nrecip rec)
{
    if (!list)
        return;
    else
    {
        struct nrecip *tmp = list;
        struct nrecip *new = malloc(sizeof(struct nrecip));
        while (tmp->next != NULL)
            tmp = tmp->next;
        new->cmd = rec.cmd;
        new->next = NULL;
        tmp->next = new;
    }
}

void add_dependency(struct ndep *list, struct ndep dep)
{
    if (!list)
        return;
    else
    {
        struct ndep *tmp = list;
        struct ndep *new = malloc(sizeof(struct ndep));
        while (tmp->next != NULL)
            tmp = tmp->next;
        new->depend = dep.depend;
        new->next = NULL;
        tmp->next = new;
    }
}

void add_rule(struct rule *list, struct rule rul)
{
    if (!list)
        return;
    if (list && !list->next && !list->target)
    {
        list->target = rul.target;
        list->dependencies = rul.dependencies;
        list->recipe = rul.recipe;
        list->done = 0;
        list->next = NULL;
    }
    else
    {
        struct rule *tmp = list;
        struct rule *new = malloc(sizeof(struct rule));
        while (tmp->next != NULL)
            tmp = tmp->next;
        new->target = rul.target;
        new->dependencies = rul.dependencies;
        new->recipe = rul.recipe;
        new->next = NULL;
        new->done = 0;
        tmp->next = new;
    }
}

void free_var(struct var *vv)
{
    if (!vv)
        return;
    free(vv->alias);
    free(vv->value);
    free_var(vv->next);
    free(vv);
}

void free_depen(struct ndep *vv)
{
    if (!vv)
        return;
    free(vv->depend);
    free_depen(vv->next);
    free(vv);
}

void free_recip(struct nrecip *vv)
{
    if (!vv)
        return;
    free(vv->cmd);
    free_recip(vv->next);
    free(vv);
}

void free_rule(struct rule *vv)
{
    if (!vv)
        return;
    free(vv->target);
    free_depen(vv->dependencies);
    free_recip(vv->recipe);
    free_rule(vv->next);
    free(vv);
}
