#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "minimake.h"
#include "noarg.h"
#include "arg.h"

/*
** This function returns:
**    -1 if an unknown options is found
**     0 if no option is found
**     1 if there is -h option (even if it is also with -f option)
**     2 if there's only -f options
*/
int sweep(int c, char *args[])
{
    int flag_h = 0;
    int flag_f = 0;
    int val = 0;
    int i = 1;
    char *elmt = NULL;

    for (; i < c; i++)
    {
        elmt = args[i];
        if (elmt[0] == '-')
        {
            if ((strcmp(elmt, "-h") != 0) && (strcmp(elmt, "-f") != 0))
                return -1;
        }
        if (!strcmp(elmt, "-h"))
            flag_h = 1;
        if (!strcmp(elmt, "-f"))
        {
            if (flag_f == 1)
                return 3;
            flag_f = 1;
            elmt = args[i + 1];
            if ((elmt == NULL) || (elmt[0] == '-'))
                val = 21;
        }
    }
    if (flag_h == 1)
        val = 1;
    else
    {
        if (flag_f == 1)
        {
            if (val != 21)
                val = 2;
        }
    }
    return val;
}

int file_ind(int c, char *args[])
{
    int i = 1;
    char *elmt = NULL;
    for (; i < c; i++)
    {
        elmt = args[i];
        if (!strcmp(elmt, "-f"))
            return i + 1;
    }
    return -1;
}

int main(int argc, char *argv[])
{
    int sw = 0;
    if ((argc == 1) && (argv != NULL))
    {//DO MINIMAKE WITHOUT ARGS
        //RETURN A VALUE
        FILE *f = choose_makefile("");
        if (!f)
        {
            fprintf(stderr, "No makefile found, please create one\n");
            return 1;
        }
        struct makefile *mf = parse_makefile(f);
        if (mf)
        {
            sw = execNoArg(mf);
            free_var(mf->variable);
            free_rule(mf->rules);
            free(mf);
            return sw;
        }
        return 1;
    }
    sw = sweep(argc, argv);
    if ((sw == -1) || (sw == 21))
    {
        fprintf(stderr, "Unknown option(s) in the command line, or bad usage ");
        fprintf(stderr, "of an option\nRun with -h options for ");
        fprintf(stderr, "more informations\n");
        return 1;
    }
    if (sw == 3)
    {
        fprintf(stderr, "Same option used more than once\n");
        return 1;
    }

    if (sw == 0)
    {// DO MINIMAKE WITH PRECISE RULES BUT NO OPTION
        //RETURN
        FILE *f = choose_makefile("");
        if (!f)
        {
            fprintf(stderr, "No makefile found, please create one\n");
            return 1;
        }
        struct makefile *mf = parse_makefile(f);
        if (mf)
        {
            sw = execArg(mf, argv);
            free_var(mf->variable);
            free_rule(mf->rules);
            free(mf);
            return sw;
        }
        return 1;
    }

    if (sw == 1)
    {
        printf("    Minimake help\n\nUsage: ./minimake [-f file][-h][rules]\n");
        return 0;
    }

    if (sw == 2)
    {
        //MINIMAKE WITH -f OPTION MAYBE PRECISE RULES
        int i = file_ind(argc, argv);
        FILE *f = choose_makefile(argv[i]);
        if (!f)
        {
            fprintf(stderr, "File \"%s\" not found\n", argv[i]);
            return 1;
        }
        struct makefile *mf = parse_makefile(f);
        if (mf)
        {
            if (argc == 3)
                sw = execNoArg(mf);
            else
                sw = execArg(mf, argv);
            free_var(mf->variable);
            free_rule(mf->rules);
            free(mf);
            return sw;
        }
        return 1;
    }

}
