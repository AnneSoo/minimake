#ifndef MINIMAKE_H
#define MINIMAKE_H

struct nrecip
{
    char *cmd;
    struct nrecip *next;
};

struct ndep
{
    char *depend;
    struct ndep *next;
};

struct rule
{
    char *target;
    struct ndep *dependencies;
    struct nrecip *recipe;
    struct rule *next;
    int done;
};

struct var
{
    char *alias;
    char *value;
    struct var *next;
};

struct makefile
{
    struct var *variable;
    struct rule *rules;
};

void init_var(struct var *vv);
void init_rec(struct nrecip *vv);
void init_dep(struct ndep *vv);
void init_rul(struct rule *vv);
void add_var(struct var *list, struct var vari);
void add_recipe(struct nrecip *list, struct nrecip rec);
void add_dependency(struct ndep *list, struct ndep dep);
void add_rule(struct rule *list, struct rule rul);
void free_var(struct var *vv);
void free_depen(struct ndep *vv);
void free_recip(struct nrecip *vv);
void free_rule(struct rule *vv);
struct makefile* parse_makefile(FILE *f);
FILE* choose_makefile(char *file);

#endif
