#ifndef NOARG_H
#define NOARG_H

int search_rule(char *target, struct rule *rl);
int exec_rule(char *target, struct rule *rl);
int execNoArg(struct makefile *mf);
int exec_cmd(char *cmd);


#endif
