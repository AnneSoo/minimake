#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "minimake.h"

void parse_var(char *name, char *c, struct makefile *mf)
{
    int i = 0;
    char *val = malloc(512 * sizeof(char));
    struct var vari;
    vari.alias = malloc(512 * sizeof(char));
    vari.value = malloc(512 * sizeof(char));
    strcpy(vari.alias, name);
    do
    {
        c++;
    } while (*c == ' ');

    while (*c != '\n')
    {
        val[i] = c[0];
        c++;
        i++;
    }
    val[i] = '\0';
    strcpy(vari.value, val);
    free(val);
    add_var(mf->variable, vari);
}

void parse_depen(char *c, struct rule *rl, char *name)
{
    strcpy(rl->target, name);
    int i = 0;
    do
    {
        c++;
    } while (*c == ' ');
    while (*c != '\n')
    {
        if (*c == '#')
            return;
        char *dep = malloc(512 * sizeof(char));
        while ((*c != ' ') && (*c != '\n') && (*c != '#'))
        {
            dep[i] = c[0];
            c++;
            i++;
        }
        dep[i] = '\0';

        struct ndep depen;
        depen.depend = malloc(512 * sizeof(char));
        strcpy(depen.depend, dep);
        add_dependency(rl->dependencies, depen);
        if (*c != '\n')
        {
            if (*c == '#')
                return;
            do
            {
                c++;
            } while (*c == ' ');
        }
        free(dep);
    }
}

void parse_cmd(int *res, char **line, size_t *len, FILE *f, struct rule *rl)
{
    char *c = NULL;
    int i = 0;
    int flag = 0;

    *res = getline(line, len, f);
    while (*res != -1)
    {
        if ((*line[0] != '\t') && (*line[0] != '\n'))
        {
            flag = 1;
            break;
        }
        else
        {
            if (*line[0] != '\n')
            {
                char *cmd = malloc(512 * sizeof(char));
                i = 0;
                c = *line;
                do
                {
                    c++;
                } while (*c == ' ');
                while (*c != '\n')
                {
                    cmd[i] = c[0];
                    c++;
                    i++;
                }
                cmd[i] = '\0';
                struct nrecip rec;
                rec.cmd = malloc(512 * sizeof(char));
                strcpy(rec.cmd, cmd);
                add_recipe(rl->recipe, rec);
                free(cmd);
            }
            do
            {
                free(*line);
                *line = NULL;
                *res = getline(line, len, f);
            } while ((*res != -1) && (*line[0] == '\n'));
        }
    }
    if (flag == 1)
         return;
}

struct makefile *parse_makefile(FILE *f)
{
    char *line = NULL;
    size_t len = 0;

    struct makefile *mf = malloc(sizeof(struct makefile));
    mf->variable = malloc(sizeof(struct var));
    init_var(mf->variable);
    mf->rules = malloc(sizeof(struct rule));
    init_rul(mf->rules);
    int res_gtl = getline(&line, &len, f);
    char *name = NULL;
    int flag = 0;
    while ((res_gtl != -1) && (line[0] == '\n'))
    {
        free(line);
        line = NULL;
        res_gtl = getline(&line, &len, f);
    }
    if (res_gtl != -1)
    {
        flag = 1;
        do
        {
            name = malloc((strlen(line) + 1) * sizeof(char));
            char *c = line;
            int i = 0;
            while ((*c != ':') && (*c != '=') && (*c != ' ') && (*c != '#')
                   && (*c != '\n'))
            {
                name[i] = c[0];
                c++;
                i++;
            }
            name[i] = '\0';
            if (*c == ' ')
            {
                do
                {
                    c++;
                } while (*c == ' ');
            }
            if (*c == '=')
            {
                parse_var(name, c, mf);
                do
                {
                    free(line);
                    line = NULL;
                    res_gtl = getline(&line, &len, f);
                } while ((res_gtl != -1) && (line[0] == '\n'));
            }

            else if (*c == ':')
            {
                struct rule rl;
                rl.target = malloc(512 * sizeof(char));
                rl.dependencies = malloc(sizeof(struct ndep));
                init_dep(rl.dependencies);
                rl.recipe = malloc(sizeof(struct nrecip));
                init_rec(rl.recipe);

                parse_depen(c, &rl, name);
                free(line);
                line = NULL;
                parse_cmd(&res_gtl, &line, &len, f, &rl);
                //add rl to makefile->rules
                add_rule(mf->rules, rl);
            }
            else
            {
                if (strcmp(name, "") != 0)
                {
                    //Syntax error, stop the program and return 1
                    fprintf(stderr, "Syntax error\n");
                    free(name);
                    free_var(mf->variable);
                    free_rule(mf->rules);
                    free(mf);
                    free(line);
                    return NULL;
                }
                if ((*c == '#') || (*c == '\n'))
                {
                    do
                    {
                        free(line);
                        line = NULL;
                        res_gtl = getline(&line, &len, f);
                    } while ((res_gtl != -1)
                             && ((line[0] == '\n') || (line[0] == '#')));
                }
            }

            free(name);
            name = NULL;
        } while (res_gtl != -1);
    }
    free(line);
    if (!flag)
    {
        printf("No rule found in the makefile\n");
        free_var(mf->variable);
        free_rule(mf->rules);
        free(mf);
        return NULL;
    }
    return mf;
}
